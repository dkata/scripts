dart test --coverage=coverage
dart pub global activate coverage
dart pub global run coverage:format_coverage --lcov -o ./coverage/lcov.info -i ./coverage
dart pub global deactivate coverage
genhtml -o ./coverage/report ./coverage/lcov.info
open ./coverage/report/index.html
